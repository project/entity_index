<?php

/**
 * @file
 * Entity index query class.
 */

/**
 * Class that extends EntityFieldQuery to provide related methods.
 */
class EntityIndexQuery extends EntityFieldQuery {

  /**
   * The ordered results, as a result of EntityFieldQuery::execute().
   */
  public $indexQueryResult = array();

  /**
   * Executes the query, storing the result in $this->indexQueryResult.
   */
  public function execute() {
    $result = parent::execute();
    $this->indexQueryResult = $result;
    return $result;
  }

  /**
   * Adds a condition on the entity index type, e.g. node or taxonomy_term.
   *
   * @param $value
   *   The value for $name. In most cases, this is a scalar. For more complex
   *   options, it is an array. The meaning of each element in the array is
   *   dependent on $operator.
   * @param $operator
   *   Possible values:
   *   - '=', '<>', '>', '>=', '<', '<=', 'STARTS_WITH', 'CONTAINS': These
   *     operators expect $value to be a literal of the same type as the
   *     column.
   *   - 'IN', 'NOT IN': These operators expect $value to be an array of
   *     literals of the same type as the column.
   *   - 'BETWEEN': This operator expects $value to be an array of two literals
   *     of the same type as the column.
   *   The operator can be omitted, and will default to 'IN' if the value is an
   *   array, or to '=' otherwise.
   *
   * @return EntityFieldQuery
   *   The called object.
   */
  public function indexCondition($value, $operator = NULL) {
    $enabled_entity_types = entity_index_enabled_entity_types();
    if (in_array($value, $enabled_entity_types)) {
      $value = 'entity_index_' . $value;
      return $this->entityCondition('entity_type', $value, $operator);
    }
    else {
      throw new EntityFieldQueryException(t('Entity type not indexed.'));
    }
  }

  /**
   * Load the results of the EntityFieldQuery via an entity_load() on the result
   * records.
   */
  public function loadResults() {
    $results = array();
    foreach ($this->indexQueryResult as $entity_type => $records) {
      $results[$entity_type] = entity_load($entity_type, array_keys($records));
    }
    return $results;
  }

}
