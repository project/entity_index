<?php

/**
 * Provide a list of fields to add to the entity index table for a given entity
 * type.
 *
 * The schema info for the given field will be automatically pulled from the
 * source table.
 *
 * @param $entity_type
 *   The entity type.
 *
 * @return array
 *   A list of fields should be returned, keyed in arrays by their source table.
 *   Optionally, provide a string value for array keys to define source and
 *   target field names. For example, pull the forum.tid field and name the
 *   index forum_tid.
 */
function hook_entity_index_fields($entity_type) {
  $fields = array();

  if ($entity_type == 'node') {
    $fields['node'] = array('uid', 'changed', 'created', 'status');

    if (module_exists('forum')) {
      $fields['forum'] = array(
        'tid' => 'forum_tid',
      );
    }
  }

  return $fields;
}

/**
 * Provide a list of extra, "virtual" fields to add to the entity index table
 * for a given entity type. This hook functions much like hook_entity_index_fields(),
 * but expects the index name as the key, and the schema definition (an array)
 * as the value for each field. This schema definition should follow Schema API.
 *
 * @param $entity_type
 *   The entity type.
 *
 * @return array
 *   A list of fields should be returned, keyed by index name, with the schema
 *   definition as the value.
 */
function hook_entity_index_extra_fields($entity_type) {
  $fields = array();

  if ($entity_type == 'node') {
    $fields['example_integer'] = array(
      'description' => 'A computed integer generated during hook_entity_index_populate_data().',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    );
  }

  return $fields;
}

/**
 * Provide a list of indexes to be created for the given table.
 *
 * This is critical for actually improving the performance of any queries on
 * entity index tables. Although entity index modules may provide some of
 * these, it is more likely that each project will implement their own set of
 * indexes according to their needs.
 *
 * @param $entity_type
 *   The entity type.
 *
 * @return array
 *   A list of fields to index, keyed by index name, with the index fields as
 *   the value. Should follow Schema API structure.
 */
function hook_entity_index_indexes($entity_type) {
  $indexes = array();

  if ($entity_type == 'node') {
    $indexes['created_changed'] = array('created', 'changed');
  }

  return $indexes;
}

/**
 * Add all relevant data to the query that rebuilds the index data.
 *
 * Note: The base table is never abbreviated, e.g. 'node' is not aliased to 'n'.
 * This is done to avoid bad assumptions on the logic used to create this alias.
 *
 * @param $entity_type
 *   The entity type.
 * @param $query
 *   The query object to modify.
 */
function hook_entity_index_populate_data($entity_type, &$query) {
  if ($entity_type == 'node') {
    $query->addField('node', 'nid', 'entity_id');
    $query->addField('node', 'type', 'bundle');
    $query->fields('node', array('uid', 'created', 'changed'));
  }
}

/**
 * Called after a full rebuild of the content in the index table.
 *
 * Use this to back-fill fields that don't have a discreet table column.
 *
 * @param string $entity_type
 *   The entity type.
 */
function hook_entity_index_post_populate($entity_type) {

}

/**
 * Called after a single entity has been populated.
 *
 * @param string $entity_type
 *   The entity type.
 * @param int $entity_id
 *   The entity's PK.
 */
function hook_entity_index_post_populate_entity($entity_type, $entity_id) {

}

/**
 * Submodules must implement this hook to ensure their entities are indexed.
 *
 * @return array
 *   An array of entity types. e.g. array('node');
 */
function hook_entity_index_enabled_entities() {

}

/**
 * Called after hook_entity_index_enabled_entities to alter the data.
 *
 * @param array $enabled_entities
 *   List of enabled entities.
 */
function hook_entity_index_enabled_entities_alter(&$enabled_entities) {

}