<?php

/**
 * @file
 * Drush integration functions for the entity_index module.
 */

/**
 * Implements hook_drush_command().
 */
function entity_index_drush_command() {
  $items['entity-index-rebuild'] = array(
    'description' => 'Rebuild the entity index schema and data.',
    'callback' => 'entity_index_drush_rebuild',
    'options' => array(
      'no-data' => array(
        'description' => 'Do not rebuild entity data immediately.',
      ),
      'data-only' => array(
        'description' => 'Do not rebuild the schema, only re-populate the data.',
      ),
    ),
    'drupal dependencies' => array('entity_index'),
  );
  return $items;
}

/**
 * Drush callback to easily rebuild the entity index schema.
 */
function entity_index_drush_rebuild() {
  // Optionally avoid re-creating the schema
  if (!drush_get_option('data-only')) {
    module_load_include('install', 'entity_index');
    _entity_index_rebuild_schema();
  }

  // Optionally avoid re-populating the data
  if (!drush_get_option('no-data')) {
    entity_index_populate_data();
  }
}
