Entity index
=============

For complex sites with a lot of content, entity indexs can be extremely
expensive. This module attempts to flatten the key information in an easily
searchable and sortable table.


Entity index API
----------------

All available hooks are documented in entity_index.api.php.


Upgrading
---------

Generally, updates can be applied by rebuilding the entity index via Drush. As
a result, update hooks will only be provided in cases where variables must be
altered or other bulk operations are required.


Drush commands
--------------

In order to rebuild the entity index tables, the following Drush comamnd can be
used -

    drush entity-index-rebuild

NOTE: This rebuilds all of the data immediately, which can be very expensive on
large sites. The `--no-data` option can be passed to skip this data rebuild (or
for testing changes during development. This has been tested on a site with 1.5M
node records and this command executed consistently approximately one minute.)

Variables
---------

entity_index_post_populate_on_request (boolean): If true, process
entity_index_post_populate implementations on page request. Otherwise, queue up
processing.

Backups
-------

Because the entity_index tables are not primary data and can become quite large, it may be preferred to remove these tables from your SQL backups (or SQL sync). In order to do this, add the following snippet to your `drushrc.php` file -

    $options['structure-tables']['entity_index'] = array('entity_index_*');
    $command_specific['sql-dump']['structure-tables-key'] = array('entity_index');
    $command_specific['sql-sync']['structure-tables-key'] = array('entity_index');
