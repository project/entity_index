<?php

/**
 * @file
 * Views integration for entity index module.
 */

/**
 * Implements hook_views_data_alter().
 */
function entity_index_views_data_alter(&$data) {
  // Setup base tables for each entity type
  foreach (entity_index_enabled_entity_types() as $entity_type) {
    // Define the base group of this table. Fields that don't
    // have a group defined will go into this field by default.
    $data["entity_index_{$entity_type}"]['table']['group'] = t('Entity index (@entity_type)', array('@entity_type' => $entity_type));

    // Advertise this table as a possible base table
    $data["entity_index_{$entity_type}"]['table']['base'] = array(
      'field' => 'id',
      'title' => t('Entity index (@entity_type)', array('@entity_type' => $entity_type)),
    );

    // Add support for node access and basic joins
    if ($entity_type == 'node') {
      $data["entity_index_{$entity_type}"]['table']['base']['access query tag'] = 'node_access';

      $data["entity_index_{$entity_type}"]['table']['default_relationship'] = array(
        'node' => array(
          'table' => 'node',
          'left_field' => 'nid',
          'field' => 'id',
        ),
      );
    }

    $tables = entity_index_get_fields($entity_type);

    // Determine the type of each fields
    foreach ($tables as $table => $fields) {
      foreach ($fields as $source_field => $target_field) {
        if (isset($data[$table])) {
          $data["entity_index_{$entity_type}"][$target_field] = $data[$table][$source_field];
          $data["entity_index_{$entity_type}"][$target_field]['help'] = t('Field %field_name from table %table_name.', array('%field_name' => $source_field, '%table_name' => $table));
        }
      }
    }
  }
}

/**
 * Implements hook_views_plugins_alter().
 */
function entity_index_views_plugins_alter(&$plugins) {
  // Add entity index tables to the base of the node row plugin.
  foreach (entity_index_enabled_entity_types() as $entity_type) {
    if (isset($plugins['row'][$entity_type])) {
      $plugins['row'][$entity_type]['base'][] = "entity_index_{$entity_type}";
    }
    if (isset($plugins['row']["{$entity_type}_rss"])) {
      $plugins['row']["{$entity_type}_rss"]['base'][] = "entity_index_{$entity_type}";
    }
  }
}
